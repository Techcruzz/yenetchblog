﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Yenetch.Entity;

namespace Admin.web
{
    public partial class blog_category : System.Web.UI.Page
    {
       
        public class query
        {
            public long Id { get; set; }
            public string Value { get; set; }
            public string action { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var db = new YenetchEntities();

            if (!IsPostBack)
                hidCategoryData.Value = JsonConvert.SerializeObject(db.blogCategories.Where(c => c.IsActive == true && c.IsDeleted == false).ToList());


            if (Request.QueryString["action"] == "Add")
                AddCategory();
            if (Request.QueryString["action"] == "Edit")
                EditCategory();
            if (Request.QueryString["action"] == "Delete")
                DeleteCategory();
        }

        public void AddCategory()
        {
            var db = new YenetchEntities();
            var query = Query();
            if (query.action == "Add" && !string.IsNullOrEmpty(query.Value))
            {

                var category = new blogCategory();
                category.Parent = query.Id;
                category.text = query.Value.ToString();
                category.DateCreated = DateTime.Now;
                category.DateModified = DateTime.Now;
                category.IsActive = true;
                category.IsDeleted = false;
                db.blogCategories.Add(category);
                db.SaveChanges();
            }
            Response.Redirect("blog-category");
        }
        public void EditCategory()
        {
            var db = new YenetchEntities();
            var query = Query();
            if (query.action == "Edit" && !string.IsNullOrEmpty(query.Value))
            {
                var category = db.blogCategories.Where(x => x.ID == query.Id).SingleOrDefault();
                category.text = query.Value.ToString();
                category.DateModified = DateTime.Now;
                db.blogCategories.Attach(category);
                db.Entry(category).State = EntityState.Modified;
                db.SaveChanges();
            }
            Response.Redirect("blog-category");
        }
        public void DeleteCategory()
        {
            var db = new YenetchEntities();
            var query = Query();
            if (query.action == "Delete" && query.Id > 0)
            {
                var category = db.blogCategories.Where(x => x.ID == query.Id).SingleOrDefault();
                db.blogCategories.Remove(category);
                db.SaveChanges();
            }
            Response.Redirect("blog-category");
        }
        public query Query()
        {
            var query = new query();
            query.Id = Convert.ToInt64(Request.QueryString["id"]);
            query.Value = Request.QueryString["value"];
            query.action = Request.QueryString["action"];
            return query;
        }
    }
}