﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Yenetch.Entity;
namespace Admin.web.Controls
{
    public partial class BlogCategoryControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var db = new YenetchEntities();
            if (!IsPostBack)
                hidCategoryData.Value = JsonConvert.SerializeObject(db.blogCategories.Where(c => c.IsActive == true && c.IsDeleted == false).ToList());
        }
    }
}