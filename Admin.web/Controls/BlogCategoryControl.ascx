﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogCategoryControl.ascx.cs" Inherits="Admin.web.Controls.BlogCategoryControl" %>


<style>
    p#tree {
        max-height: 250px;
        overflow: auto;
       // overflow-x: scroll;
    }

    .list-group {
        white-space: nowrap;
        display: block;
    }

        .list-group .list-group-item {
            padding: 6px 10px 6px 10px;
            color: #000;
            border-left: none;
            border-right: none;
            outline: none;
            border-radius: 5px;
        }

            .list-group .list-group-item:hover {
                padding: 8px 15px 8px 12px;
                background: #8cbf47;
                color: white !important;
                border-radius: 5px;
                outline: none;
            }

    .treeview ul li {
        text-transform: capitalize;
    }
</style>

<link href="Theme/categoty/bootstrap-treeview.min.css" rel="stylesheet" />
<script src="Theme/categoty/bootstrap-treeview.js"></script>
<asp:HiddenField ID="hidCategoryData" runat="server" ClientIDMode="Static" />
<p id="tree"></p>



<script>

    var hfData = JSON.parse($('#hidCategoryData').val());
    var data = getNestedChildren(hfData, 0);

    function getNestedChildren(arr, parent) {
        var out = []
        for (var i in arr) {
            if (arr[i].Parent == parent) {
                var nodes = getNestedChildren(arr, arr[i].ID)
                if (nodes.length) arr[i].nodes = nodes
                out.push(arr[i])
            }
        }
        return out
    }
    $('#tree').treeview({
        data: data,
        levels: 1,
        expandIcon: 'fa fa-plus',
        collapseIcon: 'fa fa-minus',
        emptyIcon: 'fa fa-minus',
        showCheckbox: true,
        enableLinks: false,
    });






</script>