﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="admin-login.aspx.cs" Inherits="Admin.web.admin_login1" %>




<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Admin - Login</title>
     
    <!-- Bootstrap core CSS -->
    <link href="LoginTheme/css/bootstrap.css" rel="stylesheet">
      <link href="../../MsgBox/upa-alert-msg.css" rel="stylesheet" />
    <!--external css-->
   <style>
       
/*LOGIN CONFIGURATION PAGE*/
.form-login {
	max-width: 330px;
	margin: 100px auto 0;
	background-color: rgba(234, 234, 234, 0.8);
	border-radius: 5px;
	-webkit-border-radius: 5px;
}

.form-login h2.form-login-heading {
	margin: 0;
	padding: 25px 20px;
	text-align: center;
    background: #882767;
	border-radius: 5px 5px 0 0;
	-webkit-border-radius: 5px 5px 0 0;
	color: #fff;
	font-size: 20px;
	text-transform: uppercase;
	font-weight: 300;
}
.login-wrap {
	padding: 20px;
}
.login-wrap .registration {
	text-align: center;
}
.login-social-link {
	display: block;
	margin-top: 20px;
	margin-bottom: 15px;
}

/*Theme Buttons*/

.btn-theme {
  color: #fff;
  background-color: #882767;
  border-color: #882767;
}
.btn-theme:hover,
.btn-theme:focus,
.btn-theme:active,
.btn-theme.active,
.open .dropdown-toggle.btn-theme {
  color: #fff;
  background-color: #882767;
  border-color: #882767;
}

.btn-theme02 {
  color: #fff;
  background-color: #ac92ec;
  border-color: #967adc;
}
.btn-theme02:hover,
.btn-theme02:focus,
.btn-theme02:active,
.btn-theme02.active,
.open .dropdown-toggle.btn-theme02 {
  color: #fff;
  background-color: #967adc;
  border-color: #967adc;
}

.btn-theme03 {
  color: #fff;
  background-color: #48cfad;
  border-color: #37bc9b;
}
.btn-theme03:hover,
.btn-theme03:focus,
.btn-theme03:active,
.btn-theme03.active,
.open .dropdown-toggle.btn-theme03 {
  color: #fff;
  background-color: #37bc9b;
  border-color: #37bc9b;
}

.btn-theme04 {
  color: #fff;
  background-color: #ed5565;
  border-color: #da4453;
}
.btn-theme04:hover,
.btn-theme04:focus,
.btn-theme04:active,
.btn-theme04.active,
.open .dropdown-toggle.btn-theme04 {
  color: #fff;
  background-color: #da4453;
  border-color: #da4453;
}

.btn-clear-g {
	color: #48bcb4;
	background: transparent;
	border-color: #48bcb4;
}

.btn-clear-g:hover {
	color: white;
}

hr {
  margin-top: 20px;
  margin-bottom: 20px;
  border: 0;
  border-top: 1px solid #797979;
}

.btn-facebook {
  color: #fff;
  background-color: #5193ea;
  border-color: #2775e2;
}
.btn-facebook:hover,
.btn-facebook:focus,
.btn-facebook:active,
.btn-facebook.active,
.open .dropdown-toggle.btn-facebook {
  color: #fff;
  background-color: #2775e2;
  border-color: #2775e2;
}

.btn-twitter {
  color: #fff;
  background-color: #44ccfe;
  border-color: #2bb4e8;
}
.btn-twitter:hover,
.btn-twitter:focus,
.btn-twitter:active,
.btn-twitter.active,
.open .dropdown-toggle.btn-twitter {
  color: #fff;
  background-color: #2bb4e8;
  border-color: #2bb4e8;
}

   </style>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
      
      <link href="../MsgBox/upa-alert-msg.css" rel="stylesheet" />
      
      <style>
          span#error {
              color: red;
              font-size: 11px;
              font-weight: 100;
          }

      </style>
  </head>

  <body>
      <form runat="server">
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  <div id="login-page">
	  	<div class="container">
	  	    <div class="upa-msgbox upa-fail"><asp:Label ID="lblError" runat="server" ClientIDMode="Static"></asp:Label><a class="btnclose">x</a></div>
            <div class="upa-msgbox upa-succ"><asp:Label ID="lblSuccess" runat="server" ClientIDMode="Static"></asp:Label><a class="btnclose">x</a></div>
		       <div class="form-login" >
		        <h2 class="form-login-heading">ADMIN LOGIN</h2>
                 
		        <div class="login-wrap">
		            <p class="hss-error"><asp:Label ID="error" runat="server"></asp:Label></p>
		            <asp:TextBox ID="txtUsername" type="text" placeholder="Enter Email/ Phone Number" CssClass="form-control placeholder-no-fix" runat="server"></asp:TextBox>
		            <br>
		           <asp:TextBox ID="txtPassword" type="password" placeholder="Password" CssClass="form-control placeholder-no-fix" runat="server"></asp:TextBox>
		            <label class="checkbox">
		                <span class="pull-right">
		                    <a data-toggle="modal" href="login.html#myModal"> Forgot Password?</a>
		
		                </span>
		            </label>
                    <asp:CheckBox ID ="rememberme" runat="server"  Text ="remember Password" />
		            <asp:Button ID="btnLogin" OnClick="btnLogin_Click" CssClass="btn btn-theme btn-block" type="submit" Text="SIGN IN" runat="server"></asp:Button>
		            <hr>
		            
		            <%--<div class="login-social-link centered">
		            <p>or you can sign in via your social network</p>
		                <button class="btn btn-facebook" type="submit"><i class="fa fa-facebook"></i> Facebook</button>
		                <button class="btn btn-twitter" type="submit"><i class="fa fa-twitter"></i> Twitter</button>
		            </div>--%>
		            <div class="registration">
		                Don't have an account yet?<br/>
		                <a class="" href="admin-register.aspx">
		                    Create a BUSINESS account now
		                </a>
		            </div>
		
		        </div>
		
		          <!-- Modal -->
		          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
		              <div class="modal-dialog">
		                  <div class="modal-content">
		                      <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                          <h4 class="modal-title">Forgot Password ?</h4>
		                      </div>
		                      <div class="modal-body">
		                          <p>Enter your e-mail address below to reset your password.</p>
		                          <asp:TextBox ID="txtUsernameReset" type="text" name="email" placeholder="Email" CssClass="form-control placeholder-no-fix" runat="server"></asp:TextBox>
		                          
		                      </div>
		                      <div class="modal-footer">
		                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
		                          <button class="btn btn-theme" type="button">Submit</button>
		                      </div>
		                  </div>
		              </div>
		          </div>
		          <!-- modal -->
		
		         </div>		
	  	
	  	</div>
	  </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="LoginTheme/js/jquery.js"></script>
    <script src="LoginTheme/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="LoginTheme/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("assets/img/login-bg.jpg", { speed: 500 });
    </script>
          <script src="../../MsgBox/upa-alert-msg.js"></script>
    </form>
 
  </body>
</html>
