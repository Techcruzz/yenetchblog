﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Yenetch.Entity;
namespace Admin.web
{
    public partial class blogstatics : Page
    {
        protected List<webStatic> webStatic = new List<webStatic>();
        protected List<postStatic> postStatic = new List<postStatic>();
        public List<blog> blogData = new List<blog>();
        protected void Page_Load(object sender, EventArgs e)
        {
            var db = new YenetchEntities();
            object[] o = new object[2];
            o = new BlogStaticsServices().GetCount();
            lblTodayVisit.Text = "Today Visit: " + o[0].ToString();
            lblTotalVisit.Text = "Total Visit: " + o[1].ToString();
            webStatic = db.webStatics.ToList();
            postStatic = db.postStatics.ToList();
            blogData = db.blogs.ToList();
        }
    }
}