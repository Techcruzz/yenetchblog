﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Yenetch.Entity;
namespace Admin.web
{
    public partial class admin_login1 : System.Web.UI.Page
    {
        public string email;
        public string phone;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Session["newregister"] != null)
                {
                    if (Session["newregister"].ToString() == "1")
                        lblSuccess.Text = "Your registration is successful, please login to continue.";
                    else return;
                }

                if (Request.Cookies["userid"] != null)
                    txtUsername.Text = Request.Cookies["userid"].Value;
                if (Request.Cookies["pwd"] != null)
                    txtPassword.Attributes.Add("value", Request.Cookies["pwd"].Value);
                if (Request.Cookies["userid"] != null && Request.Cookies["pwd"] != null)
                    rememberme.Checked = true;
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Session["newregister"] = "";
            lblSuccess.Text = "";
            if (txtUsername.Text == "")
            {
                lblError.Text = "Please enter username";
                txtUsername.Focus();
                return;
            }
            if (txtPassword.Text == "")
            {
                lblError.Text = "Please enter password";
                txtPassword.Focus();
                return;
            }
            email = txtUsername.Text.ToString();
            phone = txtUsername.Text.ToString();
            var db = new YenetchEntities();
            try
            {
                var data = db.users.Where(x => x.email == email || x.phone_no == phone).SingleOrDefault();
                if (data == null)
                {
                    lblError.Text = "Username and password do not match with any account";
                    return;
                }

                string pass = data.password.ToString();
                if (pass == txtPassword.Text.ToString())
                {
                    List<user> Admin = new List<user>();
                    user l = new user()
                    {
                        id = data.id,
                        username = data.username,
                        name = data.name,
                        phone_no = data.phone_no,
                        email = data.email,
                        role = data.role
                    };

                    Session["currentuser"] = l as user;
                    Authentication.Authenticate(string.Format("{0}", l.username), l.role, rememberme.Checked);

                    //Remember Me
                    if (rememberme.Checked == true)
                    {
                        Response.Cookies["userid"].Value = txtUsername.Text;
                        Response.Cookies["pwd"].Value = txtPassword.Text;
                        Response.Cookies["userid"].Expires = DateTime.Now.AddDays(15);
                        Response.Cookies["pwd"].Expires = DateTime.Now.AddDays(15);
                    }
                    else
                    {
                        Response.Cookies["userid"].Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies["pwd"].Expires = DateTime.Now.AddDays(-1);
                    }

                    if (Request.QueryString["ReturnUrl"] != null)
                    {
                        Response.Redirect(Request.QueryString["ReturnUrl"]);
                    }
                    else
                    {
                        Response.Redirect("~/dashboard.aspx", false);
                    }
                }
                else
                {
                    lblError.Text = "Username and password do not match with any account";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}