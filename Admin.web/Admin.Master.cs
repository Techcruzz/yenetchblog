﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Yenetch.Entity;
namespace Admin.web
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        public string UserName;
        public string pagename;
        protected void Page_Load(object sender, EventArgs e)
        {
            pagename = GetCurrentPageName();
            if (Session["currentuser"] == null)
            {
                Response.Redirect("un-authorize.aspx?ReturnUrl=" + Request.Url.AbsolutePath);
            }

            else
            {
                var user = Session["currentuser"] as user;
                if (user != null)
                {
                    string _userRole = Permission.Role(user.username, user.role);
                    if (_userRole != myuser.admin)
                    {
                        Response.Redirect("~/default");
                        Session["Unauthentic_User"] = user.role;
                    }
                    else if (_userRole == myuser.admin)
                    {
                        UserName = user.username;
                    }
                }

                else
                {
                    Response.Redirect("un-authorize.aspx");
                }
            }
        }

        protected void Logout_Click(object sender, EventArgs e)
        {
            Response.Redirect("logging-out");
        }
        public  string GetCurrentPageName()
        {
            string urlPath = Request.Url.AbsolutePath;
            FileInfo fileInfo = new FileInfo(urlPath);
            string pageName = fileInfo.Name;
            return pageName;
        }
    }

}