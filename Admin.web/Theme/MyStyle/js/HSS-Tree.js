﻿

/*************TREE*SCRIPT**************/

$('ul.u-tree li b').click(function (e) {
    e.stopPropagation();
    $(this).parent().children('ul.u-tree-child').slideToggle("fast");
    var haschild = $(this).children('b').children();
    if ($(this).children('b > i').text() != "-") {
        $(this).children('b > i').text("-");
        $(this).children('b > i').css("background-color", "#e8e8e8");
    }
    else {
        $(this).children('b > i').text("+");
        $(this).children('b > i').css("background-color", "#5ea96c");
    }
});

$(document).ready(function () {

    if ($('.u-tree li').has().children('a')) {
        $('.u-tree li > a').prepend('<i>-</i>');
        $('.u-tree li > a > i').css("background-color", "#e8e8e8");
    }
    if ($('.u-tree li').has().children('b')) {
        $('.u-tree li > b').prepend('<i>+</i>');
        $('.u-tree li > b > i').css("background-color", "#5ea96c");
    }

});