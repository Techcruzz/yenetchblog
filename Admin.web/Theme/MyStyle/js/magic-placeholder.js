﻿$('.placeholder-textbox').focusout(function () {
    if ($(this).val() != "") {
        $(this).addClass('correct-input');
        $(this).next('.placeholder-text').addClass('hint-show');
    } else {
        $(this).removeClass('correct-input');
        $(this).next('.placeholder-text').removeClass('hint-show');
    }
});

$(document).ready(function () {
    $("input[type=text],input[type=number],input[type=password],input[type=email],textarea").each(function () {
        if ($(this).val() != "") {
            $(this).addClass('correct-input');
            $(this).next('.placeholder-text').addClass('hint-show');
        } else {
            $(this).removeClass('correct-input');
            $(this).next('.placeholder-text').removeClass('hint-show');
        }
    });
});

$(function () {
    $('input[type=number]').on('keydown', function (e) { -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/.test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault() });
});