﻿

/*************TREE*SCRIPT**************/

$('ul.u-tree li b').click(function (e) {
    e.stopPropagation();
    $(this).parent().children('ul.u-tree-child').slideToggle("fast");
    
    var haschild = $(this).children('b').children();
    if ($(this).children('b > i').text() == "+") {
        $(this).children('b > i').text("-");
        $(this).children('b > i').css("background-color", "#e8e8e8");
    }
    else if ($(this).children('b > i').text() == "-") {
        $(this).children('b > i').text("+");
        $(this).children('b > i').css("background-color", "#5ea96c");
    }

});

$(document).ready(function () {

    if ($('.u-tree li').has().children('a')) {
        $('.u-tree li > a').prepend('<i>-</i>');
            
        $('.u-tree li > a > i').css("background-color", "#e8e8e8");
    }
    if ($('.u-tree li').has().children('b')) {
        $('.u-tree li li > b').prepend('<i>+</i>');
        $('.u-tree-child').slideUp();
        $('.u-tree li > b > i').css("background-color", "#5ea96c");
    }

    $('a.tab-navig:first-child').addClass('tab-active');
    $('ul.u-tree > li:first-child > ul.u-tree-child').addClass('show');

    $('b + label > input[type=number]').remove();
    $('b + label > i').remove();

    $('label.tree-checkbox input[type=number]').attr('disabled', true);
});

$('a.tab-navig').click(function () {
    $('a.tab-navig').removeClass('tab-active');
    var tab = $(this).data('nav');
    var tab = '#' + tab;
    $(this).addClass('tab-active');
    $('ul.u-tree > li > .u-tree-child').removeClass('show');
    $(tab).addClass('show');
    
});

$('label.tree-checkbox > span.tree-checkbox').click(function (e) {
    e.stopPropagation();
   
});


$('label.tree-checkbox > input[type=checkbox]').change(function () {
    if ($(this).is(":checked")) {
        $(this).siblings('input[type=number]').removeAttr('disabled');
        if ($(this).siblings('input[type=number]').hasClass('male')) {
            var spanId = $(this).siblings('span.tree-checkbox').data('id');
            $(this).next().next('input[type=number]').attr('id', 'm' + spanId);
        }
        if ($(this).siblings('input[type=number]').hasClass('female')) {
            var spanId = $(this).siblings('span.tree-checkbox').data('id');
            $(this).next().next().next().next('input[type=number]').attr('id', 'f' + spanId);
        }
    }

    else {
        $(this).siblings('input[type=number]').attr('disabled', true);
        $(this).siblings('input[type=number]').val('');
    }
});

function getAllServices() {
    $('#hiddenfield').val('');
    var data = [];
    var temp = '0';
    $('label.tree-checkbox input[type=number]').each(function () {
        if ($(this).val() != "") {
            var type = $(this).attr('id');
            if (type[0] == "m") {
                var temp = $(this).attr('id').substring(1, $(this).attr('id').length) + ',' + $(this).val() + ',' + 'm;';
            }

            else if (type[0] == "f") {
                var temp = $(this).attr('id').substring(1, $(this).attr('id').length) + ',' + $(this).val() + ',' + 'f;';
            }
            //var temp = 'Insert into table (service_id,price) values(' + $(this).attr('id') + ',' + $(this).val() + ');';
        }
        data.push($.trim(temp));
        $('#hiddenfield').val(data);
    });

    alert($('#hiddenfield').val());
}


function getActiveServices() {
    $('#hiddenfield').val('');

    //GetObject-ActiveXObject-TAB
    var active = $('.tab-nav-block > a.tab-active').data('nav');
    var data = [];
    var temp = '0';
    $('ul#' + active + ' label.tree-checkbox input[type=number]').each(function () {
        if ($(this).val() != "") {
            var type = $(this).attr('id');
            if (type[0] == "m") {
                var temp = $(this).attr('id').substring(1, $(this).attr('id').length) + ',' + $(this).val() + ',' + 'm;';
            }

            else if (type[0] == "f") {
                var temp = $(this).attr('id').substring(1, $(this).attr('id').length) + ',' + $(this).val() + ',' + 'f;';
            }
        }
        data.push($.trim(temp));
        $('#hiddenfield').val(data);
    });
    var ele1 = $('.tab-nav-block > a.tab-active');
    $(ele1).css('background', 'rgba(173, 222, 170, 0.45)');
    $(ele1).removeClass('tab-active');
    $(ele1).next('a.tab-navig').addClass('tab-active');
    var ele2 = $('.tab-nav-block > a.tab-active').data('nav');
    $('ul.show').removeClass('show');
    $('#' + ele2).addClass('show');
    //alert($('#hiddenfield').val());
}