﻿
/*ERROR BOX WITH CLOSE BUTTON*/
    $('.btnclose').click(function() {
        $(this).parent().fadeOut();
    });
    
/**MESSAGEBOX TO SHOW/HIDE**/
    $(document).ready(function() {
        if (!$('#lblSuccess').text().trim().length) {
            $('#lblSuccess').parent().slideUp();
        } else {
            $('#lblSuccess').parent().slideDown();
        }
    });

    function UpaMsgShow() {
        if (!$('#lblSuccess').text().trim().length) {
            $('#lblSuccess').parent().slideUp();
        } else {
            $('#lblSuccess').parent().slideDown();
        }

        if (!$('#lblError').text().trim().length) {
            $('#lblError').parent().slideUp();
        } else {
            $('#lblError').parent().slideDown();
        }
    }
    function UpaMsgShow(msgtype, msg) {
        if (msgtype == '0') {
            $('#lblError').text(msg);
            $('.upa-msgbox.upa-succ').slideUp();
            $('.upa-msgbox.upa-fail').slideDown();
        }
        else if (msgtype == '1') {
            $('#lblSuccess').text(msg);
            $('.upa-msgbox.upa-succ').slideDown();
            $('.upa-msgbox.upa-fail').slideUp();
        }
    }

    $(document).ready(function () {
        if (!$('#lblError').text().trim().length) {
            $('#lblError').parent().slideUp();
        } else {
            $('#lblError').parent().slideDown();
        }
    });
