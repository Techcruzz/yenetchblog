﻿$('#uploadmask').click(function () {
    $('#filePrescription').click();
});

function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        $('#image_upload_preview1').css('background', 'none');
        $('#image_upload_preview1').prev().prev('i').show();
        reader.onload = function (e) {
            $('#image_upload_preview1').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        $('#image_upload_preview2').css('background', 'none');
        $('#image_upload_preview2').prev().prev('i').show();
        reader.onload = function (e) {
            $('#image_upload_preview2').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
function readURL3(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        $('#image_upload_preview3').css('background', 'none');
        $('#image_upload_preview3').prev().prev('i').show();
        reader.onload = function (e) {
            $('#image_upload_preview3').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
function readURL4(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        $('#image_upload_preview4').css('background', 'none');
        $('#image_upload_preview4').prev().prev('i').show();
        reader.onload = function (e) {
            $('#image_upload_preview4').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#inputFile1").change(function () {
    readURL1(this);
});
$("#inputFile2").change(function () {
    readURL2(this);
});
$("#inputFile3").change(function () {
    readURL3(this);
});
$("#inputFile4").change(function () {
    readURL4(this);
});

window.reset = function (e) {
    e.prev().hide();
    e.next('img').removeAttr('src');
    e.next('img').removeAttr('style');
    e.wrap('<form>').closest('form').get(0).reset();
    e.unwrap();
}