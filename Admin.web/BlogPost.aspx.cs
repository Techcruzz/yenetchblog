﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web.UI;
using System.Web.UI.WebControls;
using Yenetch.Entity;
namespace Admin.web
{
    public partial class WebForm7 : System.Web.UI.Page
    {
        protected List<blog> blog;
        protected List<blogCategory> blogCategory;
        protected List<postBlogCategory> postBlogCategory;



        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["currentuser"] != null)
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["action"] == "delete" && Request.QueryString["type"] == "Post") DeletePost();
                    var db = new YenetchEntities();
                    blog = db.blogs.Where(x => x.isActive == true && x.IsDeleted == false).ToList();
                    blogCategory = db.blogCategories.Where(x => x.IsActive == true && x.IsDeleted == false).ToList();
                    postBlogCategory = db.postBlogCategories.Where(x => x.IsActive == true && x.IsDeleted == false).ToList();
                }
            }
        }

        private void DeletePost()
        {
            int QID = 0;
            int.TryParse(Request.QueryString["id"], out QID);
            if (QID > 0)
            {
                var db = new YenetchEntities();
                var temp = db.blogs.SingleOrDefault(b => b.id == QID);
                temp.IsDeleted = true;
                temp.isActive = false;
                db.blogs.Attach(temp); // update rows
                db.Entry(temp).State = EntityState.Modified;
                db.SaveChanges();
                Response.Redirect("~/BlogPost");
            }
        }
    }
}