﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="blog-statics.aspx.cs" Inherits="Admin.web.blogstatics" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContainer" runat="Server">
    <br />
    <br />
    <div class="container bg-success">
        <div class="row " style="padding:20px">
            <asp:Label ID="lblTodayVisit" runat="server" CssClass="col-sm-6"></asp:Label>

            <asp:Label ID="lblTotalVisit" runat="server" CssClass="col-sm-6"></asp:Label>
        </div>
    </div>


    <div>
        <h1 class="bg-info text-center">Web Statics by Country</h1>
        <table style="width: 100%" class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th>sr.</th>
                    <th>Country</th>
                    <th>Count</th>
                </tr>
            </thead>
            <tbody>
                <%int sr = 1; %>
                <% foreach (var d in webStatic.GroupBy(x => x.country).Select(group => new
                    {
                        Country = group.Key,
                        Count = group.Count()
                    }).OrderBy(x => x.Country))
                    {%>
                <tr>
                    <th><%= sr.ToString() %></th>
                    <th><%= d.Country %></th>
                    <th><%= d.Count %></th>
                </tr>
                <%sr++; %>
                <%}  %>
                <tr class="bg-primary">
                    <th class="text-center" colspan="3">
                        
                        <div class="row">
                        <div class="col-sm-6">Viewed By Total Countries. <strong><%=(sr - 1).ToString() %></strong> </div>
                        <div class="col-sm-6">Total Count: <strong><%= webStatic.Count().ToString() %> </strong></div></div>
                    </th>
                </tr>
            </tbody>
        </table>
        <br />
        <h1 class="bg-info text-center">Post Statics by View</h1>
        <table style="width: 100%" class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th>sr.</th>
                    <th>Post</th>
                    <th>Count</th>
                </tr>
            </thead>
            <tbody>
                <%int tr = 1; %>
                <% foreach (var d in postStatic.GroupBy(x => x.postID).Select(group => new
                    {
                        postID = group.Key,
                        Count = group.Count()
                    }))
                    {%>
                <tr>
                    <th><%= tr.ToString() %></th>
                    <th>
                        <a href="http://tblog.yenetch.com/post.aspx?blog=<%= d.postID %>" target="_blank">
                            <%= blogData.Where(x => x.id == d.postID).Select(x => x.heading).FirstOrDefault().ToString() %></a></th>
                    <th><%= d.Count %></th>
                </tr>
                <%tr++; %>
                <%}  %>
            </tbody>
        </table>

    </div>




</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FootContainer" runat="Server">
</asp:Content>
