﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="blog-category.aspx.cs" Inherits="Admin.web.blog_category" %>



<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" runat="server">
    <style>
        p#tree {
            width: 70%;
        }

        .list-group {
            white-space: nowrap;
            display: block;
        }

            .list-group .list-group-item {
                padding: 6px 10px 6px 10px;
                color: #000;
                border-left: none;
                border-right: none;
                outline: none;
                border-radius: 5px;
            }


        .treeview ul li {
            text-transform: capitalize;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContainer" runat="server">

    <div class="container">
        <div class="row">

            <h3 class="panel-body">Blog Category</h3>
            <br />



        </div>
        <div class="panel-body">
            <button type="button" id="btnAddNew" class="btn btn-warning">Add</button>
            <button type="button" id="btnEdit" class="btn btn-info">Edit</button>
            <button type="button" id="btnDelete" class="btn btn-danger">Delete</button>
            <br />
            <br />
            <p id="tree"></p>
        </div>
    </div>

    <div class="modal fade" id="alertModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Info</h4>
                </div>
                <div class="modal-body" id="alertMsg">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <span id="spanShowBtn"></span>

                </div>
            </div>
        </div>
    </div>


    <asp:HiddenField ID="hidCategoryData" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hidBackUrl" runat="server" ClientIDMode="Static" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FootContainer" runat="server">
    <link href="Theme/categoty/bootstrap-treeview.min.css" rel="stylesheet" />
    <script src="Theme/categoty/bootstrap-treeview.js"></script>

    <script>
        

        var hfData = JSON.parse($('#hidCategoryData').val());
        var data = getNestedChildren(hfData, 0);

        function getNestedChildren(arr, parent) {
            var out = []
            for (var i in arr) {
                if (arr[i].Parent == parent) {
                    var nodes = getNestedChildren(arr, arr[i].ID)
                    if (nodes.length) arr[i].nodes = nodes
                    out.push(arr[i])
                }
            }
            return out
        }
        $('#tree').treeview({
            data: data,
            levels: 1,
            expandIcon: 'glyphicon glyphicon-plus',
            collapseIcon: 'glyphicon glyphicon-minus',
            emptyIcon: 'glyphicon glyphicon-minus',
            showCheckbox: true,
            enableLinks: false,
        });

        var nodeid = [];
        var id = [];
        $('#tree').on('nodeChecked', function (event, data) {
            nodeid.push(data.nodeId);
            id.push(data.ID);
            if (nodeid.length > 1) {
                $('#tree').treeview('uncheckAll', { silent: true });
                $('#tree').treeview('checkNode', [nodeid[1], { silent: true }]);
                nodeid.splice(0, 1);
                id.splice(0, 1);
            }
            //  AddUrl();
        });

        $('#tree').on('nodeUnchecked', function (event, data) {
            if (nodeid.length == 1) {
                nodeid.splice(0, 1);
                id.splice(0, 1);
            }
        });

        $("#btnAddNew").on('click', function (event) {
            Resetmodal();
            if (id[0] === undefined) {
                $("#alertMsg").append("<label id='lblAddMsg'>This category name will be added in <b>root level<b>.</label>");
                $("#alertMsg").append("<input type='text' onkeyup='GetAddvalue()' class='form-control' id='txtModalAdd' placeholder='enter category name'>");
                $("#spanShowBtn").append("<a href='blog-category?action=Add&type=Category&id=0&value=' class='btn btn-warning' id='btnModalAdd'>Add Category</a>");
            }
            else {
                var SelectedNode = $('#tree').treeview('getNode', nodeid[0], { silent: true });
                $("#alertMsg").append("<label id='lblAddMsg'>This category name will be added under <b>" + SelectedNode.text + "<b>.</label>");
                $("#alertMsg").append("<input type='text' onkeyup='GetAddvalue()' class='form-control' id='txtModalAdd' placeholder='enter category name'>");
                $("#spanShowBtn").append("<a href='blog-category?action=Add&type=Category&id=" + id[0] + "&value=' class='btn btn-warning' id='btnModalAdd'>Add Category</a>");
            }
        });



        $("#btnEdit").on('click', function (event) {
            Resetmodal();
            if (id[0] === undefined) {
                $("#alertMsg").append("<label id='lblEditMsg'>Please select first category. that you want to edit.</label>");
            }
            else {
                var SelectedNode = $('#tree').treeview('getNode', nodeid[0], { silent: true });
                $("#alertMsg").append("<label id='lblEditMsg'>Rename category name in the textbox. After press savechanges.<br/></label> ");
                $("#alertMsg").append("<input type='text' onkeyup='GetEditvalue()'  class='form-control' id='txtModalEdit' value='" + SelectedNode.text + "'>");
                $("#spanShowBtn").append("<a href='blog-category?action=Edit&type=Category&id=" + id[0] + "' class='btn btn-info' id='btnModalEdit'>Save Changes</a>");
            }
        });

        $("#btnDelete").on('click', function (event) {
            Resetmodal();
            if (nodeid.length == 1) {
                var SelectedNode = $('#tree').treeview('getNode', nodeid[0], { silent: true });
                if (SelectedNode.nodes === undefined) {
                    $("#alertMsg").append("<label id='lblDeleteMsg'>Are You Sure to delete this category! After deleted it cannot be undone.<br /> Press close to go back.</label>");
                    $("#spanShowBtn").append("<a href='blog-category?action=Delete&type=Category&id=" + id[0] + "' class='btn btn-danger' id='btnModalDelete'>Delete</a>");
                }
                else {
                    $("#alertMsg").append("<label id='lblDeleteMsg'>This category cannote be deleted! beacause its have child categories.<br /> First delete its child categories.</label>");
                }
            }
            else {
                $("#alertMsg").append("<label id='lblDeleteMsg'>Please select first category. That you want to delete.</label>");
                return false;
            }
        });




        function GetEditvalue() {
            var tempUrl = "blog-category?action=Edit&type=Category&id=" + id[0] + "&value=" + $('#txtModalEdit').val();
            $('#btnModalEdit').attr('href', tempUrl);
        }
        function GetAddvalue() {
            if (id[0] === undefined)
                var tempUrl = "blog-category?action=Add&type=Category&id=0&value=" + $('#txtModalAdd').val();
            else
                var tempUrl = "blog-category?action=Add&type=Category&id=" + id[0] + "&value=" + $('#txtModalAdd').val();
            $('#btnModalAdd').attr('href', tempUrl);
        }

        function Resetmodal() {
            window.history.pushState({}, document.title, "/" + "blog-category");
            $("#lblAddMsg").remove();
            $("#txtModalAdd").remove();
            $("#btnModalAdd").remove();

            $("#lblEditMsg").remove();
            $("#btnModalEdit").remove();
            $("#txtModalEdit").remove();

            $("#lblDeleteMsg").remove();
            $("#btnModalDelete").remove();
            $('#alertModal').modal('show', { backdrop: 'fade' });
        }
    </script>
</asp:Content>
