﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Yenetch.Entity;

namespace Admin.web
{
    public partial class Subscribers : System.Web.UI.Page
    {
        public List<newslatter> subscribers;
        public List<newslatter> unsubscribers;
        protected void Page_Load(object sender, EventArgs e)
        {
            var db = new YenetchEntities();
            subscribers = db.newslatters.Where(x => 
            x.isSubscribe == true && x.isDelete == false
            ).ToList();

            unsubscribers= db.newslatters.Where(x =>
            x.isSubscribe == false && x.isDelete == false
            ).ToList();
        }

    }
}