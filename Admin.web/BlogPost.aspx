﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="BlogPost.aspx.cs" Inherits="Admin.web.WebForm7" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" runat="Server">
    <style>
        img.table-img {
            height: 50px;
            width: 50px;
            margin-right: 10px;
        }

        table.table-post {
            margin-top: 50px;
        }

         td {
          vertical-align:  middle !important
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContainer" runat="Server">

    <table class="table table-striped table-bordered table-post">
        <%if (blog.Count > 0)
            {%>
        <thead>
            <tr>
                <th>sr.</th>
                <th>Topic</th>
                <th>Date Created</th>
                <th>Post Categories</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <%for (int i = 0; i < blog.Count(); i++)
                {%>
            <tr>
                <td><%= i+1 %></td>
                <td>
                    <img src="<%= (!string.IsNullOrEmpty(blog[i].FeatureImgUrl) ? blog[i].FeatureImgUrl : "Theme/img/no-image.jpg") %>" class="table-img" />
                    <%=blog[i].topic %>

                </td>
                <td><%= Convert.ToDateTime(blog[i].date).ToString("dd MMMM yyyy") %></td>
                <td>
                    <% foreach (var pc in postBlogCategory.Where(c => c.PostID == blog[i].id))
                        { %>
                    <% foreach (var cn in blogCategory.Where(c => c.ID == pc.CategoryID && c.IsActive == true && c.IsDeleted == false))
                        { %>
                    <a href="http://tblog.yenetch.com/post?blog=<%=blog[i].id%>" class="btn-link" style="white-space: nowrap;"><%= cn.text %></a>
                    <span style="color: red; font-weight: bold">, </span>
                    <% }%>
                    <%} %>
                </td>


                <td>
                    <a href="<%--?id=<%= blog[i].id%>&type=delete--%>#" data-id="<%=blog[i].id %>" data-option="<%= blog[i].heading %>" class="btn_delete " onclick="goDoSomething(this);">
                         <i class="fa fa-trash-o"></i> Delete
                    </a>
                    <br />
                    <a href="write-blog.aspx?id=<%= blog[i].id%>&type=edit" class="">
                         <i class="fa fa-pencil-square-o"></i> Edit
                    </a>
                </td>
            </tr>
            <%} %>
        </tbody>
        <%}%>
    </table>

    <div class="modal fade" id="alertModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Info</h4>
                </div>
                <div class="modal-body" id="alertMsg">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <span id="spanShowBtn"></span>

                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FootContainer" runat="Server">
    <script>
        //for delete post
        function goDoSomething(identifier) {
            Resetmodal()
            $("#alertMsg").append("<label id='lblDeleteMsg'>Are you Sure! To Delete This Post ( <strong> " + $(identifier).data('option') + " </strong> ). After Deleting This Post Cannot Be Undone.<br/></label> ");
            $("#spanShowBtn").append("<a href='BlogPost?action=delete&type=Post&id=" + $(identifier).data('id') + "' class='btn btn-danger' id='btnModalDelete'>Delete</a>");
        }
        function Resetmodal() {
            $("#lblDeleteMsg").remove();
            $("#btnModalDelete").remove();
            $('#alertModal').modal('show', { backdrop: 'fade' });
        }
    </script>
</asp:Content>
