﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Subscribers.aspx.cs" Inherits="Admin.web.Subscribers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" runat="server">
    <style>
        img.table-img {
            height: 50px;
            width: 50px;
            margin-right: 10px;
        }

        table.table-post {
            margin-top: 50px;
        }

        td {
            vertical-align: middle !important
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContainer" runat="server">
    <div class="container" style="width: 95%; margin-top:50px;">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="panel panel-default " style="min-height:300px;">
                        <div class="panel-title">
                            <h2>Subscribers List</h2>
                        </div>
                        <div class="panel-body">
                            <%if (subscribers.Count > 0)
                                {%>
                            <table class="table table-striped table-bordered table-post">


                                <thead>
                                    <tr>
                                        <th>sr.</th>
                                        <th>Email</th>
                                        <th>Date Created</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%for (int i = 0; i < subscribers.Count(); i++)
    {%>
                                    <tr>
                                        <td><%= i + 1 %></td>
                                        <td><%= subscribers[i].email %></td>
                                        <td><%= Convert.ToDateTime(subscribers[i].dateAdded).ToString("dd MMMM yyyy") %></td>
                                        <td><%=  (subscribers[i].isActive == true ? "Active" : "Deactive") %></td>
                                        <td><a href="#" class=""><i class="fa fa-mail-reply"></i></a></td>
                                    </tr>
                                    <%} %>
                                </tbody>

                            </table>
                            <%}%>
                            <%else { %>
                           <h2>You have 0 Subscribers.</h2>
                            <%} %>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="panel panel-default" style="min-height:300px;">
                        <div class="panel-title">
                            <h2>Un-Subscribers List</h2>
                        </div>
                        <div class="panel-body">
                            <%if (unsubscribers.Count > 0)
                                {%>
                            <table class="table table-striped table-bordered table-post">


                                <thead>
                                    <tr>
                                        <th>sr.</th>
                                        <th>Email</th>
                                        <th>Date Created</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%for (int i = 0; i < subscribers.Count(); i++)
    {%>
                                    <tr>
                                        <td><%= i + 1 %></td>
                                        <td><%= subscribers[i].email %></td>
                                        <td><%= Convert.ToDateTime(subscribers[i].dateAdded).ToString("dd MMMM yyyy") %></td>
                                        <td><%=  (subscribers[i].isActive == true ? "Active" : "Deactive") %></td>
                                        <td><a href="#" class=""><i class="fa fa-mail-reply"></i></a></td>
                                    </tr>
                                    <%} %>
                                </tbody>

                            </table>
                            <%}%>
                            <%else { %>
                           <h2>You have 0 Unsubscribers.</h2>
                            <%} %>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FootContainer" runat="server">
</asp:Content>
