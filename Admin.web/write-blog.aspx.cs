﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Yenetch.Entity;
namespace Admin.web
{
    public partial class write_blog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["currentuser"] != null)
            {
                if (!IsPostBack)
                {
                    int qid = QID();
                    if (qid > 0 && Request.QueryString["type"].ToString() == "edit")
                    {
                        GetEditpost();
                        if (Request.QueryString["msg"] == "true")
                            lblSuccess.Text = "Blog has been Updated.";
                    }
                }
            }
        }

        private void GetEditpost()
        {
            int qid = QID();
            if (qid > 0 && Request.QueryString["type"].ToString() == "edit")
            {
                var db = new YenetchEntities();
                var post = db.blogs.Where(x => x.id == qid && x.IsDeleted == false && x.isActive == true).SingleOrDefault();
                txtHtmlBody.Text = post.content;
                txtBlogAuther.Text = post.auther;
                txtBlogHeading.Text = post.heading;
                txtBlogTopic.Text = post.topic;
                btnUploadBlogImage.Text = "Update Blog";
                imgurl.Value = !string.IsNullOrEmpty(post.FeatureImgUrl) ?
                    "../" + post.FeatureImgUrl :
                    "Theme/img/no-image.jpg";
                var previousCList = db.postBlogCategories.Where(bc => bc.PostID == qid && bc.IsActive == true && bc.IsDeleted == false).ToList() as IEnumerable<postBlogCategory>;
                Session["previousCList"] = previousCList;
                if (previousCList.Count() > 0)
                    hidEditCList.Value = string.Join(",", previousCList.Select(x => x.CategoryID));
            }
        }

        public void btnUploadBlogImage_Click(object sender, EventArgs e)
        {
            savePost();
        }
        public void savePost()
        {
            // Encode the content for storing in Sql server.
            string htmlEncoded = WebUtility.HtmlEncode(txtHtmlBody.Text);

            // Decode the content for showing on Web page.
            string original = WebUtility.HtmlDecode(htmlEncoded);
            int qid = QID();
            string action = null; action = Request.QueryString["type"];

            try
            {
                //SAVE TO BLOG DATABASE
                string FeatureImgurl = "";
                if (FUFeatureImage.HasFiles)
                {
                    string[] Path = ImageSave.SingleImage(FUFeatureImage.FileName);
                    FUFeatureImage.SaveAs(Path[0]);
                    FeatureImgurl = Path[1];
                }
                var db = new YenetchEntities();
                var blog = new blog
                {
                    content = original,
                    auther = txtBlogAuther.Text,
                    date = DateTime.UtcNow,
                    heading = txtBlogHeading.Text,
                    topic = txtBlogTopic.Text,
                    FeatureImgUrl = FeatureImgurl,
                    isActive = true,
                    IsDeleted = false
                };

                if (qid > 0)
                {
                    if (action == "edit")
                    {
                        var temp = db.blogs.SingleOrDefault(b => b.id == qid);
                        FeatureImgurl = temp.FeatureImgUrl;

                        if (FUFeatureImage.HasFiles)
                        {
                            string[] Path = ImageSave.SingleImage(FUFeatureImage.FileName);
                            FUFeatureImage.SaveAs(Path[0]); FeatureImgurl = Path[1];
                        }
                        temp.content = original;
                        temp.auther = txtBlogAuther.Text;
                        temp.heading = txtBlogHeading.Text;
                        temp.topic = txtBlogTopic.Text;
                        temp.FeatureImgUrl = FeatureImgurl;
                        db.SaveChanges();

                        List<postBlogCategory> postCategories = postBlogCategory(temp.id, hidCList.Value);
                        if (postCategories != null && postCategories.Count > 0)
                        {
                            var priviousCList = Session["PreviousCList"] as IEnumerable<postBlogCategory>;
                            var _old = priviousCList.Select(x => x.CategoryID).ToList();
                            var _new = postCategories.Select(x => x.CategoryID).ToList();

                            var _remove = _old.Except(_new).ToList();
                            var _update = _old.Intersect(_new).ToList();
                            var _add = _new.Except(_old).ToList();
                            foreach (var items in _remove)
                            {
                                var temp_1 = priviousCList.Where(x => x.CategoryID == items).FirstOrDefault();
                                db.postBlogCategories.Attach(temp_1);
                                db.postBlogCategories.Remove(temp_1); // Deleting rows
                                db.SaveChanges();  // save product category list
                            }
                            foreach (var items in _update)
                            {
                                var temp_2 = priviousCList.Where(x => x.CategoryID == items).FirstOrDefault();
                                temp_2.DateModified = DateTime.Now;
                                db.postBlogCategories.Attach(temp_2); // update rows
                                db.Entry(temp_2).State = EntityState.Modified;
                                db.SaveChanges();   // save product category list
                            }
                            foreach (var items in _add)
                            {
                                var temp_3 = postCategories.Where(x => x.CategoryID == items).FirstOrDefault();
                                temp_3.DateCreated = DateTime.Now;
                                temp_3.DateModified = DateTime.Now;
                                db.postBlogCategories.Attach(temp_3);
                                db.postBlogCategories.Add(temp_3);
                                db.SaveChanges();
                            }
                        }
                        Session["PreviousCList"] = null;
                        Response.Redirect("write-blog.aspx?id=" + qid + "&type=edit&msg=true");
                    }
                    if (action == "delete")
                    {
                        Response.Redirect("BlogPost.aspx");
                    }
                }
                else
                {
                    db.blogs.Add(blog);
                    db.SaveChanges();
                    List<postBlogCategory> postCategories = postBlogCategory(blog.id, hidCList.Value);
                    //Getting BlogCategory ids List(Blog Belong from which categories)
                    if (postCategories != null && postCategories.Count > 0)
                    {
                        foreach (var items in postCategories)
                        {
                            db.postBlogCategories.Add(items); // adding rows
                            db.SaveChanges();  // save product category list
                        }
                    }
                    lblSuccess.Text = "Blog has been added, see/edit this blog in BlogPost section.";
                }
            }
            catch (Exception ex)
            {
                lblError.Text = "Sorry, some error occured while writing your blog, Try again later.";
                error_detail_text.Text = ex.GetBaseException().ToString();
            }
        }

        private int QID()
        {
            int QID = 0;
            int.TryParse(Request.QueryString["id"], out QID);
            return QID;
        }

        public static List<postBlogCategory> lBlogCategory = new List<postBlogCategory>();
        public static List<postBlogCategory> postBlogCategory(Int64 PId, string CategoryListIDs)
        {
            lBlogCategory.Clear();
            var CategoryIDs = CategoryListIDs.Split(',').ToList();
            foreach (var items in CategoryIDs)
            {
                if (long.TryParse(items, out long CId))
                    lBlogCategory.Add(new postBlogCategory
                    {
                        PostID = PId,
                        CategoryID = CId,
                        Created_by = 0,
                        DateCreated = DateTime.Now,
                        DateModified = DateTime.Now,
                        IsActive = true,
                        IsDeleted = false
                    });
            }
            return lBlogCategory;
        }

    }
}