﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="write-blog.aspx.cs" Inherits="Admin.web.write_blog" ValidateRequest="False" %>

<%@ Register Src="~/Controls/BlogCategoryControl.ascx" TagPrefix="uc1" TagName="BlogCategoryControl" %>



<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" runat="Server">
    <link href="Theme/Plugin/Blog/blog.css" rel="stylesheet" />
    <link href="Theme/Plugin/Blog/jquery-te-1.4.0.css" rel="stylesheet" />
    <link href="Theme/MyStyle/font-awesome-4.6.3/css/font-awesome.css" rel="stylesheet" />
    <link href="Theme/textEditorFiles/site.css" rel="stylesheet" />
    <link href="Theme/textEditorFiles/richtext.min.css" rel="stylesheet" />
    <script src="Theme/textEditorFiles/jquery.richtext.js"></script>
    <script src="Theme/textEditorFiles/jquery.richtext.min.js"></script>

    <link href="Theme/MsgBox/upa-alert-msg.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContainer" runat="Server">
    <div class="upa-msgbox upa-fail">
        <asp:Label ID="lblError" runat="server" ClientIDMode="Static"></asp:Label>
        <a class="see-error" data-toggle="modal" data-target="#error_detail"><i class="fa fa-exclamation-triangle"></i>See Detail</a>
        <a class="btnclose">x</a>
    </div>
    <div class="upa-msgbox upa-succ">
        <asp:Label ID="lblSuccess" runat="server" ClientIDMode="Static"></asp:Label><a class="btnclose">x</a>
    </div>
    <div id="addimage" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Image</h4>
                </div>
                <div class="modal-body">
                    <asp:FileUpload ID="FileUpload1" runat="server" />

                    <span>Image Alignment</span><br />
                    <div class="img-btn">
                        <i class="fa fa-image"></i>
                        <input class="btn-blue" type="button" value="Left" />
                    </div>
                    <div class="img-btn">
                        <i class="fa fa-image"></i>
                        <input class="btn-blue" type="button" value="Left" />
                    </div>
                    <div class="img-btn">
                        <i class="fa fa-image"></i>
                        <input class="btn-blue" type="button" value="Left" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <div id="error_detail" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Error</h4>
                </div>
                <div class="modal-body">
                    <p>
                        <asp:Label runat="server" ID="error_detail_text"></asp:Label>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <div class="container" style="width: 95%">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <div class="form-panel text-area">
                    <h4 class="mb"><i class="fa fa-file-text" aria-hidden="true"></i>Write Blog</h4>
                    <hr />
                    <div class="col-sm-12 col-lg-12">

                        <div class="col-sm-12 col-lg-8 col-md-8">
                            <div class="form-control-wrapper">
                                <span>Blog Heading</span>
                                <asp:TextBox runat="server" ID="txtBlogHeading" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="form-control-wrapper">
                                <span>Blog Topic</span>
                                <asp:TextBox runat="server" ID="txtBlogTopic" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="form-control-wrapper">
                                <span>Blog Auther</span>
                                <asp:TextBox runat="server" ID="txtBlogAuther" CssClass="form-control"></asp:TextBox>
                            </div>
                            <div class="form-control-wrapper">
                                <asp:HiddenField ID="hidtxtHtmlBody" runat="server" ClientIDMode="Static" />
                                <asp:TextBox ID="txtHtmlBody" class="" runat="server" TextMode="MultiLine" Rows="50" ClientIDMode="Static"></asp:TextBox>
                                <script>
                                    $(document).ready(function () {
                                        $('#txtHtmlBody').richText();
                                        $('.txtHtmlBody').val(document.getElementById('hidtxtHtmlBody').value).trigger('change');
                                    });
                                </script>
                            </div>

                        </div>

                        <div class="col-sm-12 col-lg-4 col-md-4">
                            <div class="form-control-wrapper">
                                <span>Blog Category</span>
                                <br />
                                    <uc1:BlogCategoryControl runat="server" ID="BlogCategoryControl" />
                                     </div>

                             <div class="form-control-wrapper">
                                    <asp:HiddenField runat="server" ID="imgurl" ClientIDMode="Static" />
                                    <img class="test" src="Theme/img/no-image.jpg" id="imgpreview" style="height: 200px; object-fit: contain; margin-bottom: 15px;">
                                    <asp:FileUpload ID="FUFeatureImage" runat="server" AllowMultiple="false" accept=".png,.jpg,.jpeg" onchange="showpreview(this);" />
                                </div>
                            </div>

                       <%-- </div></div>--%>
                    </div>

                    <hr />
                    <div class="img-btn">
                        <i class="fa fa-file-text-o"></i>
                        <asp:Button runat="server" CssClass="btn btn-blue" ID="btnUploadBlogImage" OnClick="btnUploadBlogImage_Click" Text="Upload Blog" />
                    </div>

                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidCList" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hidEditCList" runat="server" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FootContainer" runat="Server">
    <script src="Theme/Plugin/Blog/jquery-te-1.4.0.min.js"></script>
    <script type="text/javascript" src="Theme/js/jquery-te-1.4.0.min.js" charset="utf-8"></script>
    <script>
        $('.jqte-test').jqte();

        // settings of status
        var jqteStatus = true;
        $(".status").click(function () {
            jqteStatus = jqteStatus ? false : true;
            $('.jqte-test').jqte({ "status": jqteStatus })
        });


        function addImage() {

        }
    </script>

    <%-- UloadImage --%>
    <script>
        function UploadImage() {
            $.ajax({
                type: "POST",
                url: "salon-detail.aspx/btnUploadBlogImage_Click",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function OnSuccess(response) {

        }
    </script>
    <script src="../MsgBox/upa-alert-msg.js"></script>

    <script>
        var SelectedCategoryList = [];
        $('#tree').on('nodeChecked', function (event, data) {
            SelectedCategoryList.push(data.ID)
            document.getElementById('hidCList').value = SelectedCategoryList;
        });

        $('#tree').on('nodeUnchecked', function (event, data) {
            SelectedCategoryList = jQuery.grep(SelectedCategoryList, function (value) {
                return value != data.ID;
            });
            document.getElementById('hidCList').value = SelectedCategoryList;
        });

        //this is when you edit
        $(document).ready(function () {
            $('#tree').treeview('expandAll', { silent: true });
            var EditCatList = $('#hidEditCList').val().split(",").map(Number).filter(x => !isNaN(x));
            for (var i = 0; i < EditCatList.length; i++) {
                var id = parseInt(EditCatList[i]);
                var nodeid = $("li.list-group-item.node-tree[data-cid=" + id + "]").attr("data-nodeid");
                if (typeof nodeid !== "undefined" && $.isNumeric(nodeid)) {
                    $('#tree').treeview('checkNode', [parseInt(nodeid), { silent: false }]);

                }
            }
        });


        function showpreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imgpreview').css('visibility', 'visible');
                    $('#imgpreview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(function () {
            var url = $('#imgurl').val();
            if (url.length > 0) {
                $('#imgpreview').css('visibility', 'visible');
                $('#imgpreview').attr('src', url);
            }
        });


    </script>

</asp:Content>

