﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Yenetch.Entity;

namespace Blog.web
{
    public partial class Default : Page
    {
        protected List<blog> Blog = new List<blog>();
        protected List<postStatic> PostStatics = new List<postStatic>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BlogStaticsServices ps = new BlogStaticsServices();
                var db = new YenetchEntities();
                Session["Blog"] = db.blogs.Where(x => x.IsDeleted == false && x.isActive == true).ToList();
                Session["PostStatics"] = db.postStatics.ToList();
            }
            Blog = Session["Blog"] as List<blog>;
            PostStatics = Session["PostStatics"] as List<postStatic>;
        }
    }
}