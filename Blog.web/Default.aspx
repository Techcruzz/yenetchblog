﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Blog.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Blog.web.Default" %>


<%@ Import Namespace="System.ComponentModel" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContainer" runat="Server">
    <title>Home || Happy Wings</title>
    <style>
        .blog-about * {
           // display: none;
        }

        .blog-about p:first-child {
            display: block;
            white-space: nowrap;
            width: 100%;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .img-responsive {
            margin: auto;
        }

        img.img-responsive {
            max-height: 350px;
            width: 100%;
            object-fit: cover;
        }

        div.blog-about::first-letter {
            font-size: 200%;
            font-weight: bold;
            text-transform: uppercase;
            top:3px;
            position: relative;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="article-list">
                <% if (Blog.Count > 0 && Blog != null)
                    { %>
                <% foreach (var item in Blog)
                    {%>
                <article class="post">
                    <header class="article-title text-center">
                        <h2>
                            <a href="post.aspx?blog=<%= item.id %>">
                                <%= item.heading %>
                            </a>
                        </h2>
                        <p>
                            <a href="post.aspx?blog=<%= item.id %>">
                                <%= item.topic %>
                            </a>
                        </p>


                    </header>

                    <div class="article-featured-content shadow-z-2">
                        <a href="post.aspx?blog=<%= item.id %>">
                            <img class="img-responsive" src='<%= (!string.IsNullOrEmpty(item.FeatureImgUrl) ? item.FeatureImgUrl : "Theme/img/no-image.jpg") %>' alt=""></a>
                    </div>
                    <header class="article-title text-center">
                        <p class="post-meta">
                            <%= (!string.IsNullOrEmpty(item.auther) 
                                    ? "Auther <strong>@ " + item.auther + "</strong> <br/>": "")%>
                            <%= item.date.ToString("MMMM dd yyyy") %>
                            &nbsp;<strong>&nbsp;<i class="fa fa-eye"></i>&nbsp;
                                <%= PostStatics.Where(x=> x.postID == item.id).Count().ToString() %>
                            </strong>
                        </p>
                    </header>
                    <div class="post-summary">
                        <div class="blog-about">
                            <% string content = (HttpUtility.HtmlDecode((string)(Regex.Replace(item.content, "<.*?>", String.Empty))));
                                content = content.Trim();
                            %>
                            <%if (content.Length >= 1)
                            {%>
                            <%if (content.Length <= 150)
                                {%>
                            <%= content.Substring(0, content.Length) %><span style="font-weight: bold;">.....</span>
                            <% }
                                else
                                {%>
                            <%=  content.Substring(0, 150) %><span style="font-weight: bold;">.....</span>
                            <%}
                            %>

                            <%} %>
                        </div>
                    </div>

                    <div class="post-detail-link text-center">
                        <a href="post.aspx?blog=<%= item.id %>" class="btn btn-fab btn-raised btn-material-red">
                            <i class="fa fa-plus"></i>
                            <div class="ripple-wrapper"></div>
                        </a>
                    </div>
                </article>
                <%} %>
                <%}  %>
           
                <nav role="navigation" class="navigation posts-navigation">
                    <div class="nav-links">
                        <div class="nav-previous">
                            <a href="#">Older posts</a>
                        </div>
                        <div class="nav-next">
                            <a href="#">Newer posts</a>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FootContainer" runat="Server">
</asp:Content>
