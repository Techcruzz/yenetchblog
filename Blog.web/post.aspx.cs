﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Yenetch.Entity;

namespace Blog.web
{
    public partial class BlogPost : Page
    {
        public int likes = 0;
        public int comments;
        public int blog_id;
        public string HashTag;

        //BLog details
        public string blog_title;
        public string blog_heading;
        public string blog_content;
        public string blog_date;
        public string blog_image;

        public string title;
        public string twitter_text;
        protected BlogStaticsServices ps = new BlogStaticsServices();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["blog"] != null)
            {

                if (!IsPostBack)
                {

                    ps.AddPostStaticCount((Request.QueryString["ip"]), Convert.ToInt32(Request.QueryString["blog"]));
                    sTotalView.InnerText = ps.GetPostCount(Convert.ToInt32(Request.QueryString["blog"])).ToString();
                }
                blog_id = Convert.ToInt32(Request.QueryString["blog"]);
                GetBlog();
                GetComments();
                //likes = ps.GetPostlikesCount(Convert.ToInt32(Request.QueryString["blog"]));
            }
            else
            {
                Response.Redirect("default.aspx");
            }

        }

        protected void GetBlog()
        {
            var db = new YenetchEntities();
            var blog = db.blogs.Where(x => x.id == blog_id).SingleOrDefault();
            HashTag = blog.heading.Replace(" ", "").ToLower();
            blog_title = blog.topic;
            blog_heading = blog.heading;
            blog_content = blog.content;
            blog_date = blog.date.ToString();
            blog_image = blog.FeatureImgUrl;

            Page.Title = blog_title + " | " + blog_heading;
            title = blog_title + " " + blog_heading;
            twitter_text = blog_heading;
        }

        protected void GetComments()
        {
            var db2 = new YenetchEntities();
            var commnets = db2.blog_comment.Where(x => x.blog_id == blog_id).ToList();

            rptComment.DataSource = commnets;
            rptComment.DataBind();

            comments = commnets.Count();
            //likes = comments.
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            if (txtName.Text == "")
            {
                txtName.Focus();
                return;
            }
            else if (txtEmail.Text == "")
            {
                txtEmail.Focus();
                return;
            }
            else if (txtNumber.Text == "")
            {
                txtNumber.Focus();
                return;
            }
            else if (txtComment.Text == "")
            {
                txtComment.Focus();
                return;
            }

            else
            {
                if (Request.QueryString["blog"] != null)
                {
                    blog_id = Convert.ToInt32(Request.QueryString["blog"]);
                }

                try
                {
                    var db = new YenetchEntities();
                    blog_comment blogComm = new blog_comment();
                    blogComm.blog_id = blog_id;
                    blogComm.user_id = 10;
                    blogComm.comment = txtComment.Text;
                    blogComm.date = DateTime.Now.Date;
                    blogComm.liked = false;
                    blogComm.name = txtName.Text;
                    blogComm.email = txtEmail.Text;
                    blogComm.number = txtNumber.Text;
                    db.blog_comment.Add(blogComm);
                    db.SaveChanges();
                    //DataAccess.ExecuteQuery(
                    //    "insert into comment([blog_id], [user_id], [comment], [date], [liked], [name], [email], [number]) values(@blogid, @userid, @comment, @date, @liked, @name, @email, @num)",
                    //    CommandType.Text, para1, para2, para3, para4, para5, para6, para7, para8);
                    GetComments();

                    //TextBoxes Empty
                    txtComment.Text = "";
                    txtEmail.Text = "";
                    txtName.Text = "";
                    txtNumber.Text = "";
                }

                catch (Exception ex)
                {

                }
            }
        }


        protected void ReplyThis(object sender, CommandEventArgs e)
        {
            CommentID.Value = e.CommandArgument.ToString();
        }

        protected void lbtnLikes_Click(object sender, EventArgs e)
        {
            BlogStaticsServices ps = new BlogStaticsServices();
            ps.AddPostLikesCount((Request.QueryString["ip"]), Convert.ToInt32(Request.QueryString["blog"]));
        }
    }
}