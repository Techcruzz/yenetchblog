﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Yenetch.Entity;
using Yenetch.Entity.bussiness;
namespace Blog.web
{
    public partial class Blog : System.Web.UI.MasterPage
    {
        public string pagename;
        protected void Page_Load(object sender, EventArgs e)
        {
            pagename = GetCurrentPageName();
            object[] o = new object[2];
            o = new BlogStaticsServices().GetCount();
            pTodayVisit.InnerText = "Today Visit: " + o[0].ToString();
            pTotalVisit.InnerText = "Total Visit: " + o[1].ToString();
            pActiveMember.InnerText = "Active Members: " + Application["SessionCount"].ToString();
        }

        protected void btnSubscribe_Click(object sender, EventArgs e)
        {
            bool email = IsValidEmailAddress(txtSubscribeEmail.Text.ToString());
            if (email)
            {
                var newslatter = new Yenetch.Entity.bussiness.newslatter();
                var result = newslatter.Addnewslatter(txtSubscribeEmail.Text.ToString());
                lblSubscribe.Text = result.Msg;
                lblSubscribe.Attributes.Add("class", result.Class);
                if(result.IsRowAffected == true)
                {
                    txtSubscribeEmail.Visible = false;
                    btnSubscribe.Visible = false;
                }
                else
                {
                    txtSubscribeEmail.Text = "";
                    txtSubscribeEmail.Focus();
                }
            }
            else
            {
                lblSubscribe.Text = "Please Enter vaild email Address ex: your@domain.com";
                txtSubscribeEmail.Focus();
                lblSubscribe.Attributes.Add("class", "subscribe-error");
            }
        }

        public bool IsValidEmailAddress(string email)
        {
            if (!string.IsNullOrEmpty(email) && new EmailAddressAttribute().IsValid(email))
                return true;
            else
                return false;
        }
        public string GetCurrentPageName()
        {
            string urlPath = Request.Url.AbsolutePath;
            FileInfo fileInfo = new FileInfo(urlPath);
            string pageName = fileInfo.Name;
            return pageName;
        }
    }
}