﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using Yenetch.Entity;

namespace Blog.web
{
    public class Global : System.Web.HttpApplication
    {


        protected void Application_Start(object sender, EventArgs e)
        {
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            Application["SessionCount"] = 0;
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            BlogStaticsServices ws = new BlogStaticsServices();
            ws.AddwebStaticCount(Request.QueryString["ip"]); // to change ip manually *query is no imp*
            Application.Lock();
            Application["SessionCount"] = Convert.ToInt32(Application["SessionCount"]) + 1;
            Application.UnLock();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            //var app = (HttpApplication)sender;

            //var cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            //if (cookie != null)
            //{
            //    var encryptedTicket = cookie.Value;
            //    var ticket = FormsAuthentication.Decrypt(encryptedTicket);
            //    // Get the stored user-data, in this case, our roles
            //    var userData = ticket.UserData;
            //    var roles = new[] { userData };
            //    var identity = new FormsIdentity(ticket);
            //    var user = new System.Security.Principal.GenericPrincipal(identity, roles);
            //    app.Context.User = user;
            //}

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {
            Application.Lock();
            Application["SessionCount"] = Convert.ToInt32(Application["SessionCount"]) - 1;
            Application.UnLock();
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}