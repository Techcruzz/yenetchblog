﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Blog.Master" AutoEventWireup="true" CodeBehind="post.aspx.cs" Inherits="Blog.web.BlogPost" %>




<asp:Content ID="Content1" ContentPlaceHolderID="HeadContainer" runat="Server">
    <style>
        @media only screen and (max-width: 767px) {
            .post-detail-link::after, .footer-bottom::before, .entry-bottom::before, .entry-bottom::after, .comment-title::before {
                left: 50% !important;
                margin-left: -50% !important;
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 767px) {
            .author-photo, .comment-author-img {
                margin-bottom: 30px;
                position: relative;
                width: auto;
            }
        }

        @media only screen and (max-width: 767px) {
            .author-right-info, .comment-content, .reply {
                padding-left: 0;
            }
        }

        @media only screen and (max-width: 767px) {
            .slide-text h2 {
                font-size: 45px !important;
            }
        }

        body {
            min-width: 200px;
        }

        .entry-content > p:first-child, .entry-content > p:last-child {
            font-size: 17px;
            margin-bottom: 20px;
            margin-top: 20px;
            padding: 0 0 0 20px;
            border-left: 5px solid #eee;
            border-color: #e2413a;
        }


        #background1 {
            left: 0px;
            top: 0px;
            z-index: -1; /* Ensure div tag stays behind content; -999 might work, too. */
        }

        .stretch {
            width: 100%;
            height: 100%;
        }

        .comment-title::before {
            background: none;
        }

        .comment-title {
            margin-bottom: 0px;
            font-weight: 700;
            padding-top: 25px;
            position: relative;
            text-align: center;
            text-transform: uppercase;
            font-size: 14px;
            display: inline-block;
            padding: 6px 0px;
        }

        @media only screen and (max-width: 423px) {
            .comment-title {
                width: 100% !important;
                float: left;
                text-align: center !important;
            }

            .left-tag {
                background: #eee;
                border-radius: 20px 20px 0 0;
            }

            .right-tag {
                background: #eee;
                border-radius: 0 0 20px 20px;
            }

            .conmment-info {
                background: none !important;
                display: inline-table;
            }
        }

        .left-tag {
            text-align: left;
            width: 48%;
        }

        .right-tag {
            text-align: right;
            width: 48%;
        }

        .conmment-info {
            margin-bottom: 40px;
            width: 60%;
            margin: auto;
            background: #eee;
            height: 40px;
            position: relative;
            top: -40px;
            padding: 0 20px;
            border-radius: 25px;
        }

        .comment-list {
            margin: 0 0 20px 0;
        }

        @media only screen and (max-width: 528px) {
            .conmment-info {
                width: 100%;
            }

            .entry-bottom span.post-tags {
                border-right: none;
            }
        }

        .comment-form input[type=submit] {
            height: 40px;
            padding: 0 15px;
            margin-bottom: 20px;
        }

        .btn-danger:not(.btn-link):not(.btn-flat) {
            background-color: #5bbd50;
            color: rgba(255,255,255,.84);
        }

        .btn-danger:active:not(.btn-link):not(.btn-flat) {
            background-color: #5bbd50;
        }

        .btn:active:not(.btn-link):not(.btn-flat), .btn-default:active:not(.btn-link):not(.btn-flat) {
            background-color: #da4747;
        }

        .btn-danger:hover:not(.btn-link):not(.btn-flat) {
            background-color: #5bbd50;
        }

        .btn-danger:hover:not(.btn-link):not(.btn-flat) {
            background-color: #5bbd50;
        }

        .comment-body {
            width: 100%;
        }

        span.placeholder-text {
            font-size: 14px;
            position: absolute;
            top: 16px;
            padding: 0 0px;
            opacity: 0.6;
            z-index: -1;
            transition: all ease-in-out .2s;
        }

        .hint-move {
            font-size: 11px;
            position: absolute;
            top: -10px;
            color: #009688;
        }

        #commentform .placeholder-textbox:focus + span.placeholder-text {
            font-size: 11px;
            position: absolute;
            top: -10px;
            color: #009688;
            font-weight: 700;
        }

        .hint-show {
            font-size: 11px !important;
            top: -10px !important;
            color: #009688 !important;
            font-weight: 700;
        }

            .hint-show:before {
                content: " ";
                background: green;
                position: relative;
                top: 6px;
            }

        .comment-list > .comment {
            display: inline-block;
            display: -webkit-box;
            width: 100%;
        }

        .comment-form input {
            height: 50px;
            margin-bottom: 20px;
        }

        #commentform .placeholder-textbox:not(:valid) + span.placeholder-text {
            color: red !important;
        }

        #commentform #txtName:not(:valid) + span.placeholder-text:after {
            content: " * (Enter at-least 3 characters)";
            color: red;
            font-weight: 300;
        }

        #commentform #txtEmail:not(:valid) + span.placeholder-text:after {
            content: " * (Email is not valid)";
            color: red;
            font-weight: 300;
        }

        #commentform #txtNumber:not(:valid) + span.placeholder-text:after {
            content: " * (Phone number is not valid)";
            color: red;
            font-weight: 300;
            z-index: 9999;
        }

        #commentform #txtComment:not(:valid) + span.placeholder-text:after {
            content: " * (Write 10 char. or more)";
            color: red;
            font-weight: 300;
        }

        #commentform input:hover + span.placeholder-text:after {
            transform: scale(1.5,1.5);
            color: blue;
        }

        #commentform input[type=number]::-webkit-inner-spin-button,
        #commentform input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            margin: 0;
        }

        .reply {
            background: none;
            color: #f44336;
            border: none;
            display: inline-block;
        }


        ul.share-buttons {
            list-style: none;
            padding: 0;
        }

            ul.share-buttons > li {
                display: inline-block;
            }



        #twitterbutton {
            color: white;
            font-weight: bold;
            font-size: 12px;
            background-color: #1da1f2;
            text-decoration: none;
            padding: 2.9px 6px;
            border-radius: 2px;
            position: relative;
            top: 2.7px;
        }

            #twitterbutton:hover {
                background-color: #50bdff;
            }

        .fb-xfbml-parse-ignore {
            text-decoration: none;
        }

        .share-buttons {
            display: inline-block;
        }

        .sharing-options {
            position: relative;
        }
    </style>
    <title><%=blog_heading%> || Happy Wings</title>
    <meta property="og:url" content="http://happywings.online/post?blog<%=blog_id %>" />
    <meta property="og:type" content="Happy Wings" />
    <meta property="og:title" content="<%=blog_heading%>" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="http://admin.happywings.online/<%= blog_image %>" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="fb-root"></div>
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

    </script>
    <iframe src="https://www.facebook.com/plugins/share_button.php?href=http%3A%2F%2Fhappywings.online%2Fpost%3Fblog%3D40&layout=button_count&size=small&width=77&height=20&appId" width="77" height="20" style="border: none; overflow: hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="article-list">
                <div>
                    <div class="entry-title text-center">
                        <p>
                            <%=blog_title %>
                        </p>
                        <h2>
                            <%=blog_heading%></h2>
                    </div>
                    <%-- <div id="background1" class="entry-featured-content shadow-z-2">
                        <img class="stretch img-responsive" src='<%=blog_image %>' alt="">
                    </div>--%>
                    <div class="entry-content">
                        <%=(HttpUtility.HtmlDecode(blog_content))%>
                    </div>


                </div>
                <div class="entry-bottom">
                    <div>
                        <span class="post-tags"><strong><i class="fa fa-share-alt"></i>Share with friends</strong>
                        </span>
                        <span><i class="fa fa-eye"></i><span runat="server" id="sTotalView"></span></span>

                        <span class="post-share"></span>
                        <div class="sharing-options">
                            <div class="fb-share-button"
                                data-href="http://happywings.online/post?blog=<%=blog_id %>"
                                data-layout="button_count">
                            </div>

                            <div class="share-buttons">
                                <a id="twitterbutton" class="twitter-share-button"><i class="fa fa-twitter"></i>Tweet</a>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="conmment-info">
                    <span class="comment-title left-tag"><i class="fa fa-comments"></i>
                        <%=comments%> comments 
                    </span>
                  
                    <asp:LinkButton ID="lbtnLikes" runat="server" OnClick="lbtnLikes_Click" CssClass="comment-title right-tag">
                        <i class="fa fa-thumbs-up"></i>
                          &nbsp;<%= ps.GetPostlikesCount(blog_id)%> &nbsp;Likes
                    </asp:LinkButton>
                </div>
                <%--                                                             
                <div class="post-author-meta">
                    <div class="author-photo">
                        <img class="img-responsive" src="Theme/img/eva.jpg" alt="">
                    </div>

                    <div class="author-right-info">
                        <h2>Eva Rahman</h2>
                        <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. </p>
                        <p><a href="http://wpexpand.com">http://wpexpand.com</a></p>
                    </div>
                </div>
                --%>
                <div id="comments" class="comments-area">
                    <div class="comment-area">
                        <ol class="comment-list">
                            <asp:Repeater ID="rptComment" runat="server">
                                <ItemTemplate>
                                    <li class="comment even thread-even depth-1">
                                        <article class="comment-body">
                                            <div class="comment-author-img">
                                                <img style="width: 83px;" src="images/defaultIcon.png" alt="">
                                            </div>

                                            <div class="comment-content">
                                                <h3><a><%#Eval("name") %></a> <span><i class="fa fa-clock-o"></i><%#DataBinder.Eval(Container.DataItem, "date", "{0:d MMM yyyy}") %></span></h3>
                                                <p><%#Eval("comment") %></p>
                                            </div>

                                            <%--<asp:Button OnClick="ReplyThis" runat="server" ID="btnReply" CommandArgument='<%#Eval("id") %>' Text="Reply" CssClass="reply"></asp:Button>--%>
                                        </article>
                                        <asp:Repeater ID="rptReply" runat="server">
                                            <ItemTemplate>
                                                <ul class="pull-right">
                                                    <li>Hello</li>
                                                </ul>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ol>
                        <%--<ol class="comment-list">
                        
                        <li class="comment even thread-even depth-1">
                            <div class="comment-body">
                                <div class="comment-author-img">
                                    <img src="Theme/img/avatar_2.jpg" alt="">
                                </div>

                                <div class="comment-content">
                                    <h3><a href="http://wordpress.org">Sir. Ananta Jalil</a> <span><i class="fa fa-clock-o"></i> 3 days ago</span></h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab dolore possimus dolorum quas necessitatibus molestias delectus, quibusdam iure nisi placeat quos repellat laborum quae, vel, atque molestiae consequuntur! Nemo, recusandae.</p>
                                </div>

                                <div class="reply"><a class="comment-reply-link" href="">Reply</a></div>
                            </div>
                            <!-- .comment-body -->

                            <ul class="children">
                                <li class="comment even thread-even depth-1">
                                    <div class="comment-body">
                                        <div class="comment-author-img">
                                            <img src="Theme/img/avatar_3.jpg" alt="">
                                        </div>

                                        <div class="comment-content">
                                            <h3><a href="http://wordpress.org">Borsha Khan</a> <span><i class="fa fa-clock-o"></i> 3 days ago</span></h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi, adipisci nobis! A corrupti, animi mollitia expedita hic exercitationem temporibus nam perferendis, commodi nobis. Qui excepturi, debitis voluptas cum voluptatibus minima consequuntur tempore ab. Inventore, soluta eveniet quia repudiandae ab maxime quam cum quibusdam necessitatibus dignissimos ullam at praesentium, sed illum.</p>
                                        </div>

                                        <div class="reply"><a class="comment-reply-link" href="">Reply</a></div>
                                    </div>
                                    <!-- .comment-body -->
                                </li>                                        
                            </ul>                                    
                        </li>
                    </ol>--%>
                        <!-- .comment-list -->
                    </div>
                    <div id="respond" class="comment-respond">
                        <h3 id="reply-title" class="comment-reply-title">Leave a Reply</h3>
                        <div id="commentform" class="comment-form" novalidate="">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-control-wrapper">
                                        <asp:TextBox autocomplete="off" ID="txtName" oninput="this.setCustomValidity('')"
                                            oninvalid="this.setCustomValidity('Enter valid name')" type="text" minlength="3"
                                            MaxLength="100" CssClass="form-control placeholder-textbox" runat="server" ClientIDMode="Static"></asp:TextBox>
                                        <span class="placeholder-text">Your Name</span> <span class="material-input"></span>
                                    </div>
                                    <div class="form-control-wrapper">
                                        <asp:TextBox autocomplete="off" ID="txtEmail" oninvalid="this.setCustomValidity('Enter valid email address')"
                                            oninput="this.setCustomValidity('')" type="email" CssClass="form-control placeholder-textbox"
                                            runat="server" ClientIDMode="Static"></asp:TextBox>
                                        <span class="placeholder-text">Your Email ID</span> <span class="material-input"></span>
                                    </div>
                                    <div class="form-control-wrapper">
                                        <asp:TextBox autocomplete="off" ID="txtNumber" oninvalid="this.setCustomValidity('Enter valid phone number')"
                                            oninput="this.setCustomValidity('')" type="number" min="7000000000" max="9999999999"
                                            CssClass="form-control placeholder-textbox" runat="server" ClientIDMode="Static"></asp:TextBox>
                                        <span class="placeholder-text">Your Phone Number</span> <span class="material-input"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-control-wrapper">
                                        <asp:TextBox autocomplete="off" ID="txtComment" TextMode="MultiLine" oninvalid="this.setCustomValidity('Write more than 10 characters')"
                                            oninput="this.setCustomValidity('')" name="comment" minlength="10" MaxLength="500"
                                            CssClass="form-control placeholder-textbox" Style="padding: 20px 0" runat="server"
                                            ClientIDMode="Static"></asp:TextBox>
                                        <span class="placeholder-text">Write Your Comment</span> <span class="material-input"></span>
                                    </div>
                                </div>
                            </div>
                            <p class="form-submit">
                                <asp:HiddenField ID="CommentID" runat="server" />
                                <asp:Button ID="btnSubmit" OnClick="Submit_Click" type="submit" class="btn btn-danger"
                                    Text="Submit comment" runat="server" ClientIDMode="Static"></asp:Button>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FootContainer" runat="Server">
    <script>
        $('.placeholder-textbox').focusout(function () {
            if ($(this).val() != "") {
                $(this).next('.placeholder-text').addClass('hint-show');
            } else {
                $(this).next('.placeholder-text').removeClass('hint-show');
            }
        });

        $(document).ready(function () {
            $("input,textarea").each(function () {
                if ($(this).val() != "") {
                    $(this).next('.placeholder-text').addClass('hint-show');
                } else {
                    $(this).next('.placeholder-text').removeClass('hint-show');
                }
            });
        });

        $(function () {
            $('input[type=number]').on('keydown', function (e) { -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/.test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault() });
        });

        $('.cmt-id').click(function () {
            var commntId = $(this).data('id');
            $('#<%=CommentID.ClientID %>').val(commntId);
        });

        $(document).ready(function () {
            var url = window.location.href;
            $('#twitterbutton').prop('href', "https://twitter.com/intent/tweet?text=&hashtags=<%=HashTag%>%0a<%=twitter_text %>.%0aRead more: " + url);
        });
    </script>
</asp:Content>
