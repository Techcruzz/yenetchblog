﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yenetch.Entity
{
    public class myuser
    {
        public static string customer = "customer";
        public static string salon = "salon";
        public static string admin = "admin";
        public static string guest = "guest";
    }
    public class Permission

    {
        public static string Role(string username, string role)
        {
            string _user_role;
            var db = new YenetchEntities();
            if (role.ToLower() == myuser.salon)
            {
                _user_role = db.businesses.Where(x => x.brand_name == username).Select(y => y.role).SingleOrDefault();
            }

            if (role.ToLower() == myuser.admin || role.ToLower() == myuser.customer)
            {
                _user_role = db.users.Where(x => x.username == username).Select(y => y.role).SingleOrDefault();
                return _user_role;
            }

            else
            {
                return myuser.guest;
            }
            return _user_role;
        }
    }
}
