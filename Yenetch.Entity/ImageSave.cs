﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Yenetch.Entity
{
    public class ImageSave
    {
        public static string[] SingleImage(string Filename)
        {

            string year = DateTime.Now.Year.ToString(), month = DateTime.Now.Month.ToString();
            string UniqueFileName = DateTime.Now.ToString().Replace(':', '_').Replace('/', '_').Replace(' ', '_') + "_" + Filename;
            string path = HttpContext.Current.Server.MapPath("~\\Upload\\" + year + "\\" + month + "\\Post\\images\\");
            Directory.CreateDirectory(path);
            string save_path = "Upload/" + year + "/" + month + "/Post/images/" + UniqueFileName;
            var PathList = new List<string>();
            PathList.Add(path + UniqueFileName);
            PathList.Add(save_path);
            return PathList.ToArray();
        }
    }
}
