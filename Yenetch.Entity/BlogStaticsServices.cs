﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Core.Objects;
using System.Web;/*.Current.Request.UserHostAddress.ToString()*/
namespace Yenetch.Entity
{
    public class BlogStaticsServices
    {
        public void AddwebStaticCount(string ipQuery)
        {
            webStatic ws = new webStatic();
            string ip = (String.IsNullOrEmpty(ipQuery)) ? NetworkIp() : ipQuery.ToString();
            string jsonGeoData = new WebClient().DownloadString("http://ip-api.com/json/" + ip);
            ws = JsonConvert.DeserializeObject<webStatic>(jsonGeoData);
            dynamic data = JObject.Parse(jsonGeoData);
            ws.dateCreated = DateTime.Now;
            ws.latitude = data.lat;
            ws.longitude = data.lon;
            ws.ispOrgName = data.org;
            ws.ipAddress = ip;

            using (var db = new YenetchEntities())

            {
                DateTime today = DateTime.Now.Date;
                // This code is for check unique ip per day only
#pragma warning disable CS0618 // Type or member is obsolete
                var ipData = db.webStatics.Where(a => a.ipAddress.Equals(ws.ipAddress) && EntityFunctions.TruncateTime(a.dateCreated) == today).FirstOrDefault();
                if (ipData == null)
                {
                    db.webStatics.Add(ws);
                    db.SaveChanges();
                }
            }
        }

        public void AddPostStaticCount(string ipQuery, int postId)
        {
            postStatic ps = new postStatic();
            string ip = (String.IsNullOrEmpty(ipQuery)) ? NetworkIp() : ipQuery.ToString();
            string jsonGeoData = new WebClient().DownloadString("http://ip-api.com/json/" + ip);
            ps = JsonConvert.DeserializeObject<postStatic>(jsonGeoData);
            dynamic data = JObject.Parse(jsonGeoData);
            ps.latitude = data.lat;
            ps.longitude = data.lon;
            ps.ispOrgName = data.org;
            ps.ipAddress = ip;
            ps.dateCreated = DateTime.Now;
            ps.postID = postId;
            using (var db = new YenetchEntities())

            {
                DateTime today = DateTime.Now.Date;
                // This code is for check unique ip per day only
#pragma warning disable CS0618 // Type or member is obsolete
                var ipData = db.postStatics.Where(a => a.ipAddress.Equals(ps.ipAddress) && a.postID.Equals(ps.postID) && EntityFunctions.TruncateTime(a.dateCreated) == today).FirstOrDefault();
                if (ipData == null)
                {
                    db.postStatics.Add(ps);
                    db.SaveChanges();
                }
            }
        }

        public object[] GetCount()
        {
            object[] o = new object[2];
            using (YenetchEntities dc = new YenetchEntities())
            {
                // coment on
                //dsfjldsjflj
                DateTime today = DateTime.Now.Date;
                // get Today Hits
                o[0] = dc.webStatics.Where(a => EntityFunctions.TruncateTime(a.dateCreated) == today).Count();

                // get all hits
                o[1] = dc.webStatics.Count();
            }
            return o;
        }
        public int GetPostCount(int postId)
        {
            int postCount = 0;
            using (YenetchEntities db = new YenetchEntities())
            {
                DateTime today = DateTime.Now.Date;
                // get all hits
                postCount = db.postStatics.Where(c => c.postID == postId).Count();
            }
            return postCount;
        }
        public Int64 GetPostlikesCount(int postId)
        {
            Int64 postCount = 0;
            using (YenetchEntities db = new YenetchEntities())
            {
                var likes = db.postLikes.Where(c => c.postId == postId).FirstOrDefault();
                postCount = likes.count;
            }
            return postCount;
        }

        public static string NetworkIp()
        {
            string localNetworkIp = "";
            if (HttpContext.Current.Request.UserHostAddress.Length > 0)
            {
                localNetworkIp = HttpContext.Current.Request.UserHostAddress.ToString();
            }
            return localNetworkIp;
        }


        public void AddPostLikesCount(string ipQuery, int postId)
        {

            uniqLikeByIP unqLikes = new uniqLikeByIP();
            string ip = (String.IsNullOrEmpty(ipQuery)) ? NetworkIp() : ipQuery.ToString();
            string jsonGeoData = new WebClient().DownloadString("http://ip-api.com/json/" + ip);
            unqLikes = JsonConvert.DeserializeObject<uniqLikeByIP>(jsonGeoData);
            dynamic data = JObject.Parse(jsonGeoData);
            unqLikes.latitude = data.lat;
            unqLikes.longitude = data.lon;
            unqLikes.ispOrgName = data.org;
            unqLikes.ipAddress = ip;
            unqLikes.dateCreated = DateTime.Now;
            unqLikes.postID = postId;
            using (var db = new YenetchEntities())

            {
                DateTime today = DateTime.Now.Date;
                // This code is for check unique ip per day only
                // Type or member is obsolete

                var ipData = db.uniqLikeByIPs.Where(a => a.ipAddress.Equals(unqLikes.ipAddress) && a.postID.Equals(unqLikes.postID) && EntityFunctions.TruncateTime(a.dateCreated) == today).FirstOrDefault();
                if (ipData == null)
                {
                    db.uniqLikeByIPs.Add(unqLikes);
                    int save = db.SaveChanges();
                    if (save == 1)
                    {
                        var likes = db.postLikes.SingleOrDefault(x => x.postId == postId);
                        if (likes == null)
                        {
                            postLike pl = new postLike();
                            pl.postId = postId;
                            pl.daateupdated = DateTime.Now;
                            pl.count = 1;
                            db.postLikes.Add(pl);
                            db.SaveChanges();
                        }
                        else
                        {
                            likes.postId = postId;
                            likes.daateupdated = DateTime.Now;
                            likes.count += 1;
                            db.SaveChanges();
                        }
                    }
                }
            }
        }

    }
}
