﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yenetch.Entity.bussiness
{
    public class newslatter
    {
        public class newslatterMsg
        {
            public string Msg { get; set; }
            public bool IsRowAffected { get; set; }
            public string Class { get; set; }
        }
        public newslatterMsg Addnewslatter(string email)
        {
            newslatterMsg n = new newslatterMsg();
            Entity.newslatter data = new Entity.newslatter();
            var db = new YenetchEntities();
            var getData = db.newslatters.Where(x => x.email == email).SingleOrDefault();
            Entity.newslatter subscribe = new Entity.newslatter();
            if (getData != null)
            {
                n.IsRowAffected = false;
                n.Msg = "You Are Already in our Subscribe List.";
                n.Class = "subscribe-success";
            }
            else
            {
                data.email = email;
                data.dateAdded = DateTime.Now;
                data.dateModified = DateTime.Now;
                data.isActive = true;
                data.isDelete = false;
                data.isSubscribe = true;
                db.newslatters.Add(data);
               int count =  db.SaveChanges();
                if (count == 1)
                {
                    n.IsRowAffected = true;
                    n.Msg = "Thank you! You are in our Subscribe List.";
                    n.Class = "subscribe-success";
                }
                else
                {
                    n.IsRowAffected = false;
                    n.Msg = "Something Went Wrong. Please try again.";
                    n.Class = "subscribe-error";
                }
            }
            return n;
        }

    }
}
