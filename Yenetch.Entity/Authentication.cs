﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace Yenetch.Entity
{
   public class Authentication
    {
        //Business Authentication
        public static void Authenticate(string username, string role, bool isPermanentCookie)
        {
            var expirationtime = DateTime.Now.AddMinutes(2880);
            var ticket = new FormsAuthenticationTicket(
                1, // Ticket version
                username, // Username to be associated with this ticket
                DateTime.Now, // Date/time ticket was issued
                expirationtime, // Date and time the cookie will expire
                false, // if user has chcked rememebr me then create persistent cookie
                role, // store the user data, in this case roles of the user
                FormsAuthentication.FormsCookiePath);
            // To give more security it is suggested to hash it
            var hashCookies = FormsAuthentication.Encrypt(ticket);
            var cookie = new System.Web.HttpCookie(FormsAuthentication.FormsCookieName, hashCookies); // Hashed ticket

            // Add the cookie to the response, user browser
            System.Web.HttpContext.Current.Response.Cookies.Add(cookie);

        }
    }
}
